import { Component, Input } from '@angular/core';

import { Echo } from './echo';
import { EchoService } from './echo.service';

@Component({
    selector: 'echo-form',
    templateUrl: './echo.component.html'
})

export class EchoComponent {
    echo = new Echo ('');
    submitted = false;

    constructor (
        private service: EchoService
    ) {}

    onSubmit() {
        this.submitted = true;
        this.service.doGet(this.echo);
    }

    newEcho() {
        this.echo = new Echo ('');
    }
}
export class Echo {
    constructor(
        public text: string,
        public echo?: string
    ) {}
}
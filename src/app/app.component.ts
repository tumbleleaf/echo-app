import { Component, Input } from '@angular/core';

import { Echo } from './echo';
import { EchoService } from './echo.service';

@Component({
  selector: 'app-root',
  template: '<echo-form></echo-form>',
  styleUrls: ['./app.component.css']
})
export class AppComponent {}

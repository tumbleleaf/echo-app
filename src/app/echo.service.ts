import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Echo } from './echo';

@Injectable()
export class EchoService {
    constructor(private http: Http) {}
    
    doGet(echo: Echo): void {
        //console.log('before GET');

        //let params: URLSearchParams = new URLSearchParams();
        //params.set('text', echo.text);
                
        this.http.get('https://80cikzz0p0.execute-api.us-east-1.amazonaws.com/v1/echo?text='+echo.text)
        //this.http.get('https://80cikzz0p0.execute-api.us-east-1.amazonaws.com/v1/echo', {search: params})
        .map((res: Response) => res.json())
            .subscribe((r: any) => {
                echo.echo = r.echo;
        });
        
        //console.log('after GET');
    }
}